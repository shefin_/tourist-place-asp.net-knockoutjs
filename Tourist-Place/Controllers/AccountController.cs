﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Tourist_Place.Data;
using Tourist_Place.Models;

namespace Tourist_Place.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly AppDbContext _context;

        public AccountController(ILogger<AccountController> logger, AppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Login()
        {
            if(User.Identity != null && User.Identity.IsAuthenticated)
                RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("UserName", "Password")] User user)
        {
            if (string.IsNullOrEmpty(user.UserName) || string.IsNullOrEmpty(user.Password))
            {
                return RedirectToAction("Login");
            }

            if (ModelState.IsValid)
            {
                var UserInDb = _context.Users.Where(u => u.UserName == user.UserName).FirstOrDefault();

                if (UserInDb == null)
                {
                    ViewData["login-error-msg"] = "User Name or Password Doesn't Exist!";
                    return View();
                }

                if(UserInDb.Password != user.Password)
                {
                    ViewData["login-error-msg"] = "User Name or Password Doesn't Exist!";
                    return View();
                }

                return LoggedIn(UserInDb);
            }

            return View();
        }

        //[HttpPost]
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        public IActionResult SignUp()
        {
            if (User.Identity != null && User.Identity.IsAuthenticated)
                RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp([Bind("UserName", "Password")] User user)
        {
            if (string.IsNullOrEmpty(user.UserName) || string.IsNullOrEmpty(user.Password))
            {
                return RedirectToAction("SignUp");
            }

            if(ModelState.IsValid)
            {
                var UserInDb = _context.Users.Where(u => u.UserName == user.UserName).FirstOrDefault();

                if (UserInDb != null) {
                    ViewData["username-error-msg"] = "User Name Exists!";
                    return View();
                }

                user.IsAdmin = false;
                _context.Users.Add(user);
                await _context.SaveChangesAsync();

                return LoggedIn(user);
            }
            

            return View();
        }

        private IActionResult LoggedIn(User user)
        {
            string Role = user.IsAdmin ? "Admin" : "User";

            var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Role, Role)
                }, CookieAuthenticationDefaults.AuthenticationScheme);

            var principal = new ClaimsPrincipal(identity);

            var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            return RedirectToAction("Index", "Home");
        }

        public string AccessDenied()
        {
            return "Access Denied! You are not admin!";
        }
    }
}
