﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using Tourist_Place.Data;
using Tourist_Place.Models;

namespace Tourist_Place.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly AppDbContext _context;

        public HomeController(ILogger<HomeController> logger, AppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View(_context.Places.ToList());
        }

        [Authorize, HttpPost]
        public async Task<IActionResult> Index(string searchPlace)
        {
            var Places = from places in _context.Places
                         select places;

            if (!string.IsNullOrEmpty(searchPlace))
                Places = Places.Where(p => p.Name.Contains(searchPlace));

            _logger.LogInformation(searchPlace);
            List<Place> PlacesList = await Places.ToListAsync();

            return PartialView("_SearchResult", PlacesList);
        }

        // GET: Home/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Places == null)
            {
                return NotFound();
            }

            var place = await _context.Places.FindAsync(id);
            if (place == null)
            {
                return NotFound();
            }

            var placeOwner = await _context.Users.FindAsync(place.UserId);

            if(placeOwner == null)
            {
                return NotFound();
            }

            if (placeOwner.UserName != User.Identity.Name && User.IsInRole("User"))
            {
                return RedirectToAction("AccessDenied");
            }

            return View(place);
        }

        // POST: Home/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Address,ImgUrl,Rating,Type, NewImage")] Place place)
        {
            if (id == null || _context.Places == null)
            {
                return NotFound();
            }

            var placeInDb = await _context.Places.FindAsync(id);
            if (placeInDb == null)
            {
                return NotFound();
            }

            var placeOwner = await _context.Users.FindAsync(placeInDb.UserId);

            if (placeOwner == null)
            {
                return NotFound();
            }

            if(placeOwner.UserName != User.Identity.Name && User.IsInRole("User"))
            {
                return RedirectToAction("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    placeInDb.Name = place.Name;
                    placeInDb.Address = place.Address;
                    placeInDb.Rating = place.Rating;
                    placeInDb.Type = place.Type;

                    if (place.NewImage != null)
                        placeInDb.ImgUrl = place.NewImage;

                    _context.Update(placeInDb);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index));
            }

            return View(placeInDb);
        }

        // GET: Home/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize, HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Address,Rating,Type,ImgUrl")] Place place)
        {
            if (ModelState.IsValid)
            {
                var PlaceOwner = _context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);

                if (PlaceOwner == null)
                    return NotFound();

                place.UserId = PlaceOwner.Id;
                _context.Add(place);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(place);
        }

        // POST: /Delete/5
        [Authorize(Roles = "Admin"), HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int? id)
        {
            if (_context.Places == null)
            {
                return Problem("Entity set 'Places' is null.");
            }
            if(id == null)
            {
                return NotFound();
            }

            var place = await _context.Places.FindAsync(id);
            if (place != null)
            {
                _context.Places.Remove(place);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        public string AccessDenied()
        {
            return "Access Denied!";
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
