﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tourist_Place.Models
{
    public class Place
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        public string Address { get; set; }

        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }

        [Required]
        [MaxLength(15)]
        public string Type { get; set; }

        [Required]
        [Display(Name= "Image")]
        public string ImgUrl { get; set; }

        [Required]
        public int UserId { get; set; }

        [NotMapped]
        [Display(Name = "Image")]
        public string? NewImage { get; set; }
    }
}
