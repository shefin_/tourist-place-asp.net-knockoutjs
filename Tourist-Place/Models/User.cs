﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Tourist_Place.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required, MaxLength(15)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required, MaxLength(50)]
        public string Password { get; set; }

        [Required]
        public bool IsAdmin { get; set; }
    }
}
